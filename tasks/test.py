id = 10

'''
if id == 10:
    message = 'True'
else:
    message = 'False'
'''

message = 'True' if id == 10 else 'False'

print(message)


